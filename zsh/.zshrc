# Lines configured by zsh-newuser-install
HISTFILE=~/.config/zsh/histfile
HISTSIZE=10000
SAVEHIST=10000
setopt autocd extendedglob nomatch
unsetopt beep notify
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/jakob/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

# Path locations
export PATH="$HOME/.local/bin:$HOME/.cabal/bin:$HOME/.ghcup/bin:$HOME/.emacs.d/bin:$PATH"

# Go path
export GOPATH="$HOME/.go"

# Export defualts
export EDITOR="nvim"

# Pure promt dubplicate letters fix
export LANG=en_US.UTF-8
export LC_CTYPE=en_US.UTF-8
 
# Pure color theme
autoload -U promptinit; promptinit
prompt pure
 
# NNN cd on quit
if [ -f /usr/share/nnn/quitcd/quitcd.bash_zsh ]; then
    source /usr/share/nnn/quitcd/quitcd.bash_zsh
fi

# Make NNN use thrash instead of delete (needs trash-cli)
export NNN_TRASH=1
 
# Load syntax highlighting and autosuggestions
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
